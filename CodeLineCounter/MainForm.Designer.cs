﻿namespace CodeLineCounter {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.codeFileSelectionGroupBox = new System.Windows.Forms.GroupBox();
            this.otherFilesGroupBox = new System.Windows.Forms.GroupBox();
            this.otherCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.webFilesGroupBox = new System.Windows.Forms.GroupBox();
            this.webFileCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.codeFilesGroupBox = new System.Windows.Forms.GroupBox();
            this.codeFileCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.countCodeLinesButton = new System.Windows.Forms.Button();
            this.resultsGroupBox = new System.Windows.Forms.GroupBox();
            this.resultsDataGridView = new System.Windows.Forms.DataGridView();
            this.lineCountAmountLabel = new System.Windows.Forms.Label();
            this.lineCountLabel = new System.Windows.Forms.Label();
            this.selectProjectFolderGroupBox = new System.Windows.Forms.GroupBox();
            this.projectFolderPathLabel = new System.Windows.Forms.Label();
            this.findProjectButton = new System.Windows.Forms.Button();
            this.ExcludeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FileNameColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.LineCountColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.codeFileSelectionGroupBox.SuspendLayout();
            this.otherFilesGroupBox.SuspendLayout();
            this.webFilesGroupBox.SuspendLayout();
            this.codeFilesGroupBox.SuspendLayout();
            this.resultsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDataGridView)).BeginInit();
            this.selectProjectFolderGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.codeFileSelectionGroupBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.countCodeLinesButton);
            this.splitContainer1.Panel2.Controls.Add(this.resultsGroupBox);
            this.splitContainer1.Panel2.Controls.Add(this.selectProjectFolderGroupBox);
            this.splitContainer1.Size = new System.Drawing.Size(792, 573);
            this.splitContainer1.SplitterDistance = 157;
            this.splitContainer1.TabIndex = 0;
            // 
            // codeFileSelectionGroupBox
            // 
            this.codeFileSelectionGroupBox.Controls.Add(this.otherFilesGroupBox);
            this.codeFileSelectionGroupBox.Controls.Add(this.webFilesGroupBox);
            this.codeFileSelectionGroupBox.Controls.Add(this.codeFilesGroupBox);
            this.codeFileSelectionGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.codeFileSelectionGroupBox.Location = new System.Drawing.Point(0, 0);
            this.codeFileSelectionGroupBox.Name = "codeFileSelectionGroupBox";
            this.codeFileSelectionGroupBox.Size = new System.Drawing.Size(153, 569);
            this.codeFileSelectionGroupBox.TabIndex = 0;
            this.codeFileSelectionGroupBox.TabStop = false;
            this.codeFileSelectionGroupBox.Text = "Select File Types";
            // 
            // otherFilesGroupBox
            // 
            this.otherFilesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.otherFilesGroupBox.Controls.Add(this.otherCheckedListBox);
            this.otherFilesGroupBox.Location = new System.Drawing.Point(6, 381);
            this.otherFilesGroupBox.Name = "otherFilesGroupBox";
            this.otherFilesGroupBox.Size = new System.Drawing.Size(137, 175);
            this.otherFilesGroupBox.TabIndex = 6;
            this.otherFilesGroupBox.TabStop = false;
            this.otherFilesGroupBox.Text = "Other Files";
            // 
            // otherCheckedListBox
            // 
            this.otherCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.otherCheckedListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.otherCheckedListBox.CheckOnClick = true;
            this.otherCheckedListBox.FormattingEnabled = true;
            this.otherCheckedListBox.Location = new System.Drawing.Point(6, 19);
            this.otherCheckedListBox.Name = "otherCheckedListBox";
            this.otherCheckedListBox.Size = new System.Drawing.Size(120, 137);
            this.otherCheckedListBox.TabIndex = 2;
            this.otherCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.otherCheckedListBox_ItemCheck);
            // 
            // webFilesGroupBox
            // 
            this.webFilesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webFilesGroupBox.Controls.Add(this.webFileCheckedListBox);
            this.webFilesGroupBox.Location = new System.Drawing.Point(6, 200);
            this.webFilesGroupBox.Name = "webFilesGroupBox";
            this.webFilesGroupBox.Size = new System.Drawing.Size(137, 175);
            this.webFilesGroupBox.TabIndex = 6;
            this.webFilesGroupBox.TabStop = false;
            this.webFilesGroupBox.Text = "Web Files";
            // 
            // webFileCheckedListBox
            // 
            this.webFileCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webFileCheckedListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.webFileCheckedListBox.CheckOnClick = true;
            this.webFileCheckedListBox.FormattingEnabled = true;
            this.webFileCheckedListBox.Location = new System.Drawing.Point(6, 19);
            this.webFileCheckedListBox.Name = "webFileCheckedListBox";
            this.webFileCheckedListBox.Size = new System.Drawing.Size(120, 137);
            this.webFileCheckedListBox.TabIndex = 1;
            this.webFileCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.webFileCheckedListBox_ItemCheck);
            // 
            // codeFilesGroupBox
            // 
            this.codeFilesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.codeFilesGroupBox.Controls.Add(this.codeFileCheckedListBox);
            this.codeFilesGroupBox.Location = new System.Drawing.Point(6, 19);
            this.codeFilesGroupBox.Name = "codeFilesGroupBox";
            this.codeFilesGroupBox.Size = new System.Drawing.Size(137, 175);
            this.codeFilesGroupBox.TabIndex = 5;
            this.codeFilesGroupBox.TabStop = false;
            this.codeFilesGroupBox.Text = "Code Files";
            // 
            // codeFileCheckedListBox
            // 
            this.codeFileCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.codeFileCheckedListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.codeFileCheckedListBox.CheckOnClick = true;
            this.codeFileCheckedListBox.FormattingEnabled = true;
            this.codeFileCheckedListBox.Location = new System.Drawing.Point(6, 19);
            this.codeFileCheckedListBox.Name = "codeFileCheckedListBox";
            this.codeFileCheckedListBox.Size = new System.Drawing.Size(120, 137);
            this.codeFileCheckedListBox.TabIndex = 0;
            this.codeFileCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.codeFileCheckedListBox_ItemCheck);
            // 
            // countCodeLinesButton
            // 
            this.countCodeLinesButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.countCodeLinesButton.Enabled = false;
            this.countCodeLinesButton.Location = new System.Drawing.Point(14, 84);
            this.countCodeLinesButton.Name = "countCodeLinesButton";
            this.countCodeLinesButton.Size = new System.Drawing.Size(603, 23);
            this.countCodeLinesButton.TabIndex = 2;
            this.countCodeLinesButton.Text = "Count";
            this.countCodeLinesButton.UseVisualStyleBackColor = true;
            this.countCodeLinesButton.Click += new System.EventHandler(this.countCodeLinesButton_Click);
            // 
            // resultsGroupBox
            // 
            this.resultsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsGroupBox.Controls.Add(this.resultsDataGridView);
            this.resultsGroupBox.Controls.Add(this.lineCountAmountLabel);
            this.resultsGroupBox.Controls.Add(this.lineCountLabel);
            this.resultsGroupBox.Location = new System.Drawing.Point(14, 113);
            this.resultsGroupBox.Name = "resultsGroupBox";
            this.resultsGroupBox.Size = new System.Drawing.Size(603, 443);
            this.resultsGroupBox.TabIndex = 6;
            this.resultsGroupBox.TabStop = false;
            this.resultsGroupBox.Text = "Results";
            // 
            // resultsDataGridView
            // 
            this.resultsDataGridView.AllowUserToAddRows = false;
            this.resultsDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.resultsDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.resultsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ExcludeColumn,
            this.FileNameColumn,
            this.LineCountColumn});
            this.resultsDataGridView.Location = new System.Drawing.Point(9, 19);
            this.resultsDataGridView.Name = "resultsDataGridView";
            this.resultsDataGridView.Size = new System.Drawing.Size(591, 405);
            this.resultsDataGridView.TabIndex = 3;
            this.resultsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.resultsDataGridView_CellContentClick);
            this.resultsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.resultsDataGridView_CellValueChanged);
            this.resultsDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.resultsDataGridView_CurrentCellDirtyStateChanged);
            // 
            // lineCountAmountLabel
            // 
            this.lineCountAmountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lineCountAmountLabel.AutoSize = true;
            this.lineCountAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lineCountAmountLabel.Location = new System.Drawing.Point(100, 427);
            this.lineCountAmountLabel.Name = "lineCountAmountLabel";
            this.lineCountAmountLabel.Size = new System.Drawing.Size(77, 13);
            this.lineCountAmountLabel.TabIndex = 2;
            this.lineCountAmountLabel.Text = "set@runtime";
            // 
            // lineCountLabel
            // 
            this.lineCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lineCountLabel.AutoSize = true;
            this.lineCountLabel.Location = new System.Drawing.Point(6, 427);
            this.lineCountLabel.Name = "lineCountLabel";
            this.lineCountLabel.Size = new System.Drawing.Size(88, 13);
            this.lineCountLabel.TabIndex = 1;
            this.lineCountLabel.Text = "Total Line Count:";
            // 
            // selectProjectFolderGroupBox
            // 
            this.selectProjectFolderGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectProjectFolderGroupBox.Controls.Add(this.projectFolderPathLabel);
            this.selectProjectFolderGroupBox.Controls.Add(this.findProjectButton);
            this.selectProjectFolderGroupBox.Location = new System.Drawing.Point(14, 19);
            this.selectProjectFolderGroupBox.Name = "selectProjectFolderGroupBox";
            this.selectProjectFolderGroupBox.Size = new System.Drawing.Size(603, 59);
            this.selectProjectFolderGroupBox.TabIndex = 5;
            this.selectProjectFolderGroupBox.TabStop = false;
            this.selectProjectFolderGroupBox.Text = "Select Solution/Project Folder";
            // 
            // projectFolderPathLabel
            // 
            this.projectFolderPathLabel.AutoSize = true;
            this.projectFolderPathLabel.Location = new System.Drawing.Point(77, 28);
            this.projectFolderPathLabel.Name = "projectFolderPathLabel";
            this.projectFolderPathLabel.Size = new System.Drawing.Size(66, 13);
            this.projectFolderPathLabel.TabIndex = 1;
            this.projectFolderPathLabel.Text = "set@runtime";
            // 
            // findProjectButton
            // 
            this.findProjectButton.Location = new System.Drawing.Point(9, 23);
            this.findProjectButton.Name = "findProjectButton";
            this.findProjectButton.Size = new System.Drawing.Size(62, 23);
            this.findProjectButton.TabIndex = 0;
            this.findProjectButton.Text = "Find";
            this.findProjectButton.UseVisualStyleBackColor = true;
            this.findProjectButton.Click += new System.EventHandler(this.findProjectButton_Click);
            // 
            // ExcludeColumn
            // 
            this.ExcludeColumn.HeaderText = "Exclude";
            this.ExcludeColumn.Name = "ExcludeColumn";
            this.ExcludeColumn.Width = 50;
            // 
            // FileNameColumn
            // 
            this.FileNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FileNameColumn.HeaderText = "File Name";
            this.FileNameColumn.Name = "FileNameColumn";
            this.FileNameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FileNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FileNameColumn.Width = 79;
            // 
            // LineCountColumn
            // 
            this.LineCountColumn.HeaderText = "Line Count";
            this.LineCountColumn.Name = "LineCountColumn";
            this.LineCountColumn.Width = 85;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Code Line Counter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.codeFileSelectionGroupBox.ResumeLayout(false);
            this.otherFilesGroupBox.ResumeLayout(false);
            this.webFilesGroupBox.ResumeLayout(false);
            this.codeFilesGroupBox.ResumeLayout(false);
            this.resultsGroupBox.ResumeLayout(false);
            this.resultsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDataGridView)).EndInit();
            this.selectProjectFolderGroupBox.ResumeLayout(false);
            this.selectProjectFolderGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox codeFileSelectionGroupBox;
        private System.Windows.Forms.GroupBox codeFilesGroupBox;
        private System.Windows.Forms.CheckedListBox codeFileCheckedListBox;
        private System.Windows.Forms.GroupBox webFilesGroupBox;
        private System.Windows.Forms.CheckedListBox webFileCheckedListBox;
        private System.Windows.Forms.GroupBox otherFilesGroupBox;
        private System.Windows.Forms.CheckedListBox otherCheckedListBox;
        private System.Windows.Forms.GroupBox selectProjectFolderGroupBox;
        private System.Windows.Forms.Label projectFolderPathLabel;
        private System.Windows.Forms.Button findProjectButton;
        private System.Windows.Forms.GroupBox resultsGroupBox;
        private System.Windows.Forms.Label lineCountAmountLabel;
        private System.Windows.Forms.Label lineCountLabel;
        private System.Windows.Forms.DataGridView resultsDataGridView;
        private System.Windows.Forms.Button countCodeLinesButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ExcludeColumn;
        private System.Windows.Forms.DataGridViewLinkColumn FileNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineCountColumn;
    }
}