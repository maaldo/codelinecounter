﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace CodeLineCounter {
    public partial class MainForm : Form {
        #region Constructors
        public MainForm() {
            InitializeComponent();

            CodeFileTypes = new List<string>();
            GlobalCodeFileLineCount = 0;

            projectFolderPathLabel.Text = string.Empty;
            lineCountAmountLabel.Text = string.Empty;

            LoadFileTypes();
        }
        #endregion

        #region Properties
        public List<string> CodeFileTypes { get; set; }

        public DirectoryInfo ProjectDirectoryInfo { get; set; }

        public int GlobalCodeFileLineCount { get; set; }
        #endregion

        #region Methods
        private void UpdateCodeFileList(CheckedListBox checkedListBox, ItemCheckEventArgs e) {
            switch (e.NewValue) {
                case CheckState.Checked:
                    CodeFileTypes.Add(checkedListBox.Items[e.Index].ToString());
                    break;
                case CheckState.Unchecked:
                    CodeFileTypes.Remove(checkedListBox.Items[e.Index].ToString());
                    break;
            }
        }

        private void UpdateCodeLineCountRecursively(DirectoryInfo directory) {
            foreach (var file in directory.GetFiles()) {
                if (!CodeFileTypes.Contains("*" + file.Extension)) { continue; }

                var individualCodeFileLineCount = 0;
                using (var sr = new StreamReader(file.FullName)) {
                    String line;
                    // Read and display lines from the file until the end of
                    // the file is reached.
                    while ((line = sr.ReadLine()) != null) {
                        if (string.IsNullOrEmpty(line)) { continue; }
                        GlobalCodeFileLineCount++;
                        individualCodeFileLineCount++;
                    }
                }
                resultsDataGridView.Rows.Add(false, file.FullName.Replace(ProjectDirectoryInfo.FullName, "..."), individualCodeFileLineCount.ToString(CultureInfo.InvariantCulture));
            }

            foreach (var dir in directory.GetDirectories()) {
                UpdateCodeLineCountRecursively(dir);
            }
        }

        private void LoadFileTypes() {
            var codeFiles = ConfigurationManager.AppSettings.Get("CodeFiles").Split(';').ToList();
            var webFiles = ConfigurationManager.AppSettings.Get("WebFiles").Split(';').ToList();
            var otherFiles = ConfigurationManager.AppSettings.Get("OtherFiles").Split(';').ToList();

            codeFiles.Sort();
            webFiles.Sort();
            otherFiles.Sort();

            codeFiles.ForEach(cf => codeFileCheckedListBox.Items.Add(string.Format("*.{0}", cf), false));
            webFiles.ForEach(wf => webFileCheckedListBox.Items.Add(string.Format("*.{0}", wf), false));
            otherFiles.ForEach(of => otherCheckedListBox.Items.Add(string.Format("*.{0}", of), false));
        }
        #endregion

        #region Event Handlers
        private void MainForm_Load(object sender, EventArgs e) { }

        private void codeFileCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e) { UpdateCodeFileList(codeFileCheckedListBox, e); }

        private void webFileCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e) { UpdateCodeFileList(webFileCheckedListBox, e); }

        private void otherCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e) { UpdateCodeFileList(otherCheckedListBox, e); }

        private void findProjectButton_Click(object sender, EventArgs e) {
            using (var folderDialog = new FolderBrowserDialog()) {
                if (folderDialog.ShowDialog() == DialogResult.OK) {
                    ProjectDirectoryInfo = new DirectoryInfo(folderDialog.SelectedPath);
                    projectFolderPathLabel.Text = ProjectDirectoryInfo.FullName;
                    countCodeLinesButton.Enabled = true;
                }
            }

            countCodeLinesButton.Enabled = true;
        }

        private void countCodeLinesButton_Click(object sender, EventArgs e) {
            GlobalCodeFileLineCount = 0;

            resultsDataGridView.Rows.Clear();

            UpdateCodeLineCountRecursively(ProjectDirectoryInfo);

            lineCountAmountLabel.Text = GlobalCodeFileLineCount.ToString(CultureInfo.InvariantCulture);
        }

        private void resultsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            // handle launch source file
            if (!e.ColumnIndex.Equals(1)) { return; }

            var file = resultsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Replace("...", ProjectDirectoryInfo.FullName);
            System.Diagnostics.Process.Start(file);
        }

        private void resultsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e) {
            if (resultsDataGridView.Columns[e.ColumnIndex].Name != "ExcludeColumn") { return; }

            var excludeFile = Convert.ToBoolean(resultsDataGridView.Rows[e.RowIndex].Cells[0].Value);
            var codeLinesInFile = Convert.ToInt32(resultsDataGridView.Rows[e.RowIndex].Cells[2].Value);

            if (excludeFile) {
                GlobalCodeFileLineCount -= codeLinesInFile;
            }
            else {
                GlobalCodeFileLineCount += codeLinesInFile;
            }

            lineCountAmountLabel.Text = GlobalCodeFileLineCount.ToString(CultureInfo.InvariantCulture);
        }

        private void resultsDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e) {
            if (resultsDataGridView.IsCurrentCellDirty) {
                resultsDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        #endregion
    }
}