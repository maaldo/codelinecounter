﻿using System;
using System.Windows.Forms;

namespace CodeLineCounter {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            try {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            catch (Exception ex) {
                // ReSharper disable once LocalizableElement
                MessageBox.Show(ex.ToString(), "Exception Detail");
                if (ex.InnerException != null) {
                    // ReSharper disable once LocalizableElement
                    MessageBox.Show(ex.InnerException.ToString(), "Inner Exception Detail");
                }
            }
        }
    }
}
